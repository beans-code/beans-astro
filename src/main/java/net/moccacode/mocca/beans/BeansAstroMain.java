package net.moccacode.mocca.beans;

import java.io.IOException;

import net.beanscode.bulk.BeansBulkMain;
import net.hypki.libs5.db.db.weblibs.ValidationException;

public class BeansAstroMain {
	public static void main(String[] args) throws IOException, ValidationException {
//		args = new String[] {"api", "--api", "api/admin/statusXX"};
		
		BeansBulkMain.main(args);
	}
}
